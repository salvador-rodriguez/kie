# Version: 0.0.1

FROM ubuntu:14.04
MAINTAINER Salvador Rodriguez <salvador.rodriguez@utah.edu>

# this is a non-interactive automated build - avoid some warning messages
ENV DEBIAN_FRONTEND noninteractive

# Install packages
ENV REFRESHED_AT 2015-03-30
RUN apt-get update && \
    apt-get install -yq --no-install-recommends mysql-server-5.6 git maven wget unzip pwgen ca-certificates && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# ssh 
RUN apt-get update && apt-get install -y openssh-server
RUN mkdir /var/run/sshd
RUN echo 'root:screencast' | chpasswd
RUN sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

# Install Java
RUN echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main"\
    > /etc/apt/sources.list.d/webupd8team-java.list \
    && echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main"\
    >> /etc/apt/sources.list.d/webupd8team-java.list \
    && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886 \
    && apt-get update -y \
    && echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections \
    && apt-get install -y oracle-java8-installer \
    && apt-get autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set java home	
RUN echo JAVA_HOME=/usr/lib/jvm/java-8-oracle >> ~/.bashrc
RUN echo PATH=$JAVA_HOME/bin:$PATH:$HOME/bin >> ~/.bashrc
RUN echo export PATH JAVA_HOME >> ~/.bashrc

# Set maven home
RUN echo export M2_HOME=/usr/share/maven >> ~/.bashrc

# Install Tomcat
ENV TOMCAT_MAJOR_VERSION 7
ENV TOMCAT_MINOR_VERSION 7.0.57
ENV CATALINA_HOME /tomcat

RUN wget -q https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR_VERSION}/v${TOMCAT_MINOR_VERSION}/bin/apache-tomcat-${TOMCAT_MINOR_VERSION}.tar.gz && \
    wget -qO- https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR_VERSION}/v${TOMCAT_MINOR_VERSION}/bin/apache-tomcat-${TOMCAT_MINOR_VERSION}.tar.gz.md5 | md5sum -c - && \
    tar zxf apache-tomcat-*.tar.gz && \
    rm apache-tomcat-*.tar.gz && \
    mv apache-tomcat* ${CATALINA_HOME}

ADD tomcat-users.xml ${CATALINA_HOME}/conf/tomcat-users.xml
ADD setenv.sh ${CATALINA_HOME}/bin/setenv.sh

# Install MySQL
ADD config_mysql.sh /config_mysql.sh
RUN chmod 755 /config_mysql.sh
RUN /config_mysql.sh
RUN rm config_mysql.sh
ADD my.cnf /etc/mysql/my.cnf

# Add mysql connector
ENV MYSQL_CONN_VERSION 5.1.26
RUN wget -P ${CATALINA_HOME}/lib/ http://central.maven.org/maven2/mysql/mysql-connector-java/${MYSQL_CONN_VERSION}/mysql-connector-java-${MYSQL_CONN_VERSION}.jar

# Set runnable scripts
RUN cd /tomcat/bin/; chmod +x *.sh

# Add bitronix
ENV BTM_VERSION 2.1.4
RUN wget -P ${CATALINA_HOME}/lib/ http://central.maven.org/maven2/org/codehaus/btm/btm/${BTM_VERSION}/btm-${BTM_VERSION}.jar

# Add btm-tomcat 5.5+
ENV BTM_TOMCAT_VERSION 2.1.4
RUN wget -P ${CATALINA_HOME}/lib/ http://central.maven.org/maven2/org/codehaus/btm/btm-tomcat55-lifecycle/${BTM_TOMCAT_VERSION}/btm-tomcat55-lifecycle-${BTM_TOMCAT_VERSION}.jar
	
# Add h2
#ENV H2_VERSION 1.3.161
#RUN wget -P /${CATALINA_HOME}/lib/ http://central.maven.org/maven2/com/h2database/h2/${H2_VERSION}/h2-${H2_VERSION}.jar

# Add jta
ENV JTA_VERSION 1.1
RUN wget -P ${CATALINA_HOME}/lib/ http://central.maven.org/maven2/javax/transaction/jta/${JTA_VERSION}/jta-${JTA_VERSION}.jar

# Add slf4j-api
ENV SLF4J_API_VERSION 1.7.2
RUN wget -P ${CATALINA_HOME}/lib/ http://central.maven.org/maven2/org/slf4j/slf4j-api/${SLF4J_API_VERSION}/slf4j-api-${SLF4J_API_VERSION}.jar

# Add slf4j-jdk14
ENV SLF4J_JDK14_VERSION 1.7.2
RUN wget -P ${CATALINA_HOME}/lib/ http://central.maven.org/maven2/org/slf4j/slf4j-jdk14/${SLF4J_JDK14_VERSION}/slf4j-jdk14-${SLF4J_JDK14_VERSION}.jar

# Add jacc_api
ENV JACC_API_VERSION 1.4
RUN wget -P ${CATALINA_HOME}/lib/ http://central.maven.org/maven2/javax/security/jacc/javax.security.jacc-api/${JACC_API_VERSION}/javax.security.jacc-api-${JACC_API_VERSION}.jar

# Add Tomcat configuration files
ADD btm-config.properties ${CATALINA_HOME}/conf/btm-config.properties
ADD resources.properties ${CATALINA_HOME}/conf/resources.properties
ADD setenv.sh ${CATALINA_HOME}/bin/setenv.sh
ADD tomcat-users.xml ${CATALINA_HOME}/conf/tomcat-users.xml
ADD server.xml ${CATALINA_HOME}/conf/server.xml

# Install drools workbench
ENV KIE_VERSION 6.2.0.Final
RUN wget -P ${CATALINA_HOME}/lib/ https://repository.jboss.org/nexus/content/groups/public/org/kie/kie-tomcat-integration/${KIE_VERSION}/kie-tomcat-integration-${KIE_VERSION}.jar
RUN wget https://repository.jboss.org/nexus/content/groups/public/org/kie/kie-wb-distribution-wars/${KIE_VERSION}/kie-wb-distribution-wars-${KIE_VERSION}-tomcat7.war
RUN unzip kie-wb-distribution-wars-${KIE_VERSION}-tomcat7.war -d ${CATALINA_HOME}/webapps/kie-wb
RUN rm kie-wb-distribution-wars-${KIE_VERSION}-tomcat7.war
ADD persistence.xml ${CATALINA_HOME}/webapps/kie-wb/WEB-INF/classes/META-INF/persistence.xml

# Add opencds-guvnor.properties
ADD opencds-guvnor.properties /root/.opencds/opencds-guvnor.properties

# Install OpenCDS
ADD opencds-decision-support-service.war ${CATALINA_HOME}/webapps/opencds-decision-support-service.war
ADD opencds.properties /root/.opencds/opencds.properties
ADD opencds-knowledge-repository-data.jar /opencds-knowledge-repository-data.jar
RUN unzip opencds-knowledge-repository-data.jar -d /root/.opencds/opencds-knowledge-repository-data/
RUN rm opencds-knowledge-repository-data.jar
ADD sec.xml /root/.opencds/sec.xml

# Add opencds dependencies
ADD opencds-common-2.1.0-SNAPSHOT.jar /opencds-common-2.1.0-SNAPSHOT.jar
ADD opencds-vmr-1_0-internal-2.1.0-SNAPSHOT.jar /opencds-vmr-1_0-internal-2.1.0-SNAPSHOT.jar
RUN mvn install:install-file -Dfile=/opencds-common-2.1.0-SNAPSHOT.jar -DgroupId=org.opencds -DartifactId=opencds-common -Dversion=2.1.0-SNAPSHOT -Dpackaging=jar
RUN mvn install:install-file -Dfile=/opencds-vmr-1_0-internal-2.1.0-SNAPSHOT.jar -DgroupId=org.opencds -DartifactId=opencds-vmr-1_0-internal -Dversion=2.1.0-SNAPSHOT -Dpackaging=jar
RUN rm opencds-vmr-1_0-internal-2.1.0-SNAPSHOT.jar
RUN rm opencds-common-2.1.0-SNAPSHOT.jar

# Add drools6-adapter example kb
ADD drools6-adapter-1.0.0.jar /drools6-adapter-1.0.0.jar
RUN mvn install:install-file -Dfile=/drools6-adapter-1.0.0.jar -DgroupId=org.opencds -DartifactId=drools6-adapter -Dversion=1.0.0 -Dpackaging=jar
RUN rm drools6-adapter-1.0.0.jar

# Add VOLUMES to allow backup of config and databases
VOLUME  ["/etc/mysql", "/var/lib/mysql", "/tomcat/webapps", "/root/.opencds"]

# Add image scripts
ADD /run.sh /usr/bin/run.sh
RUN chmod 755 /usr/bin/run.sh

EXPOSE 8080
EXPOSE 8001
EXPOSE 9418
EXPOSE 22
EXPOSE 1043

CMD ["/usr/bin/run.sh"]

