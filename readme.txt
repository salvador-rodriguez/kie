Build image :
sudo docker build -t="opencds/kie:ubuntu" .

Create container (e.g.,) :
sudo docker run --name kie -i -t opencds/kie:ubuntu 
sudo docker run -p 8080:8080 --name kie -i -t opencds/kie:ubuntu /bin/bash
sudo docker run -p 8080:8080 -p 8001:8001 --name kie -i -t opencds/kie:ubuntu /bin/bash
sudo docker run -d -p 8080:8080 -p 8001:8001 -p 9418:9418 -p 2222:22 -p 1043:1043 --name kie opencds/kie:ubuntu

In order to link the container with another container running opencds_apelon service (e.g.,):
sudo docker run -d -p 8080:8080 -p 8001:8001 -p 9418:9418 -p 2222:22 -p 1043:1043 --name kie --link opencds_apelon:oa opencds/kie:ubuntu

Check kie-drools in your browser (e.g.,) :
http://localhost:8080/kie-wb/

Notes:
Tomcat debug mode is set on port 1043, this can be disable removing the line "-agentlib:jdwp=transport=dt_socket,address=1043,server=y,suspend=n" on file setenv.sh

ssh access
user root
password screencast
