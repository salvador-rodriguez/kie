#!/bin/bash

__mysql_config() {
echo "Running the mysql_config function."
chown -R mysql:mysql /var/lib/mysql
/etc/init.d/mysql start
sleep 10
}

__start_mysql() {
echo "Running the start_mysql function."
mysqladmin -u root password mysqlPassword
mysql -uroot -pmysqlPassword -e "CREATE DATABASE jbpm CHARACTER SET UTF8;"
mysql -uroot -pmysqlPassword -e "CREATE USER 'jbpm'@'localhost' IDENTIFIED BY 'jbpm';"
mysql -uroot -pmysqlPassword -e "GRANT ALL PRIVILEGES ON jbpm.* TO 'jbpm'@'%';"
mysql -uroot -pmysqlPassword -e "GRANT ALL PRIVILEGES ON jbpm.* TO 'jbpm'@'localhost';"
mysql -uroot -pmysqlPassword -e "FLUSH PRIVILEGES;"	
killall mysqld
sleep 10
}

# Call all functions
__mysql_config
__start_mysql
