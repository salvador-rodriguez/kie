#!/bin/bash

# run ssh 
__start_ssh(){
echo "Running ssh"
/usr/sbin/sshd
}

# start mysql
__start_mysql(){
echo "Running mysql"
/etc/init.d/mysql start
}

# start tomcat
__start_tomcat(){
echo "Running tomcat"
sh /tomcat/bin/catalina.sh run
}

# Call all functions
__start_ssh
__start_mysql
__start_tomcat

